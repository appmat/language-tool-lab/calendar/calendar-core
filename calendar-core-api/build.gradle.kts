import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlin_version: String by project
val jackson_version: String by project
val ktor_version: String by project

plugins {
    application
    kotlin("jvm")
    id("org.openapi.generator") version "5.4.0"
}

group = "org.appmat"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

repositories {
    mavenCentral()
}

dependencies {
    // Ktor
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-serialization:$ktor_version")
}

tasks.withType<KotlinCompile>() {
    dependsOn += tasks.openApiGenerate
}

buildscript {
    repositories {
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("org.openapitools:openapi-generator-gradle-plugin:5.4.0")
    }
}

apply(plugin = "org.openapi.generator")

openApiGenerate {
    // https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/kotlin-server.md
    packageName.set("org.appmat.calendar.core.api")
    generatorName.set("kotlin")
    inputSpec.set("${projectDir}/../calendar-core-server/src/main/resources/openapi.yaml")
    outputDir.set("$buildDir/generated")
    configOptions.set(
        mapOf(
            "enumPropertyNaming" to "original",
            "library" to "multiplatform",
            "dateLibrary" to "java8",
            "collectionType" to "list",
        )
    )
}

configure<SourceSetContainer> {
    named("main") {
        java.srcDir("$buildDir/generated/src/main/kotlin")
    }
}
