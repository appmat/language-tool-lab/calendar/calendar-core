import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val koin_version: String by project
val ktor_version: String by project
val kotest_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometeus_version: String by project
val jackson_version: String by project
val hikari_version: String by project
val postgresql_version: String by project
val jooq_version: String by project
val kotlinx_serialization: String by project


plugins {
    application
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("org.liquibase.gradle")
}

group = "org.appmat"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    // Ktor
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")
    implementation("io.ktor:ktor-metrics-micrometer:$ktor_version")
    implementation("io.ktor:ktor-metrics:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometeus_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")

    // Health check
    implementation("com.github.zensum:ktor-health-check:011a5a8")

    // Koin
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")
    testImplementation("io.insert-koin:koin-test:$koin_version")
    testImplementation("io.insert-koin:koin-test-junit5:$koin_version")

    // Kotest
    testImplementation(platform("io.kotest:kotest-bom:$kotest_version"))
    testImplementation("io.kotest:kotest-runner-junit5")
    testImplementation("io.kotest:kotest-assertions-core")
    testImplementation("io.kotest:kotest-property")
    testImplementation("io.kotest.extensions:kotest-extensions-koin:1.1.0")
    testImplementation("io.kotest.extensions:kotest-assertions-ktor:1.0.3")

    // Testcontainer
    testImplementation("io.kotest.extensions:kotest-extensions-testcontainers:1.3.3")
    testImplementation("org.testcontainers:postgresql:1.16.3")

    // Exposed
    implementation("org.jetbrains.exposed:exposed-core:0.34.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.34.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.34.1")
    implementation("org.jetbrains.exposed:exposed-java-time:0.34.1")

    // JWT https://ktor.io/docs/jwt.html
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")

    // Serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinx_serialization")

    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.2")

    implementation("org.postgresql:postgresql:$postgresql_version")
    implementation("com.zaxxer:HikariCP:$hikari_version")

    // Liquibase
    liquibaseRuntime("org.liquibase:liquibase-core:3.4.0")
    liquibaseRuntime("org.postgresql:postgresql:$postgresql_version")
    testImplementation("org.liquibase:liquibase-core:4.9.1")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
        freeCompilerArgs = freeCompilerArgs + "-Xopt-in=io.ktor.locations.KtorExperimentalLocationsAPI"
    }
}

liquibase {
    activities.register("main") {
        val url: String = System.getProperty("liquibase.url")
        this.arguments = mapOf(
            "changeLogFile" to "${rootProject.projectDir}/liquibase/dbchangelog.xml",
            "url" to System.getProperty("liquibase.url"),
            "username" to System.getProperty("liquibase.username"),
            "password" to System.getProperty("liquibase.password"),
        )
    }
    runList = "main"
}

/**
 * stage task is executed in buildpack jvm pipeline
 * @see <a href="https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku">https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku</a>
 */
tasks.register("stage") {
    dependsOn("installDist")
    /**
     * generates the Procfile that specifies a command to run the application
     */
    doLast {
        val relativeInstallationPath = tasks.installDist.get().destinationDir.relativeTo(rootProject.projectDir)
        File("${rootProject.projectDir}/Procfile").writeText(
            """
                web: ./$relativeInstallationPath/bin/${project.name}
            """.trimIndent()
        )
    }
}

