package org.appmat.calendar.core.common.data

import java.time.LocalDate
import java.time.OffsetTime
import kotlin.time.Duration
import kotlin.time.toJavaDuration
import kotlinx.serialization.Serializable
import org.appmat.calendar.core.common.util.Util.duration
import org.appmat.calendar.core.common.util.Util.max
import org.appmat.calendar.core.plugins.serialization.TimeIntervalSerializer

@Serializable(TimeIntervalSerializer::class)
data class TimeInterval(
    val startTime: OffsetTime,
    val duration: Duration,
) {
    companion object {
        fun of(startTime: OffsetTime, endTime: OffsetTime) = TimeInterval(
            startTime = startTime,
            duration = duration(startTime, endTime)
        )

        fun List<TimeInterval>.merged(): List<TimeInterval> {
            if (this.isEmpty()) return this

            val result = mutableListOf<TimeInterval>()

            val sorted = this.sortedBy { it.startTime }

            val firstTimeInterval = sorted.first()
            var start = firstTimeInterval.startTime
            var end = firstTimeInterval.endTime()

            for (timeInterval in sorted) {
                if (timeInterval.startTime <= end) {
                    end = max(end, timeInterval.endTime())
                } else {
                    result.add(of(startTime = start, endTime = end))
                    start = timeInterval.startTime
                    end = timeInterval.endTime()
                }
            }

            result.add(of(startTime = start, endTime = end))

            return result
        }
    }

    fun endTime(): OffsetTime = startTime.plus(duration.toJavaDuration())

    fun atDate(date: LocalDate) = DateTimeInterval(startDateTime = startTime.atDate(date), duration = duration)
}
