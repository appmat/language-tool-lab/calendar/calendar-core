package org.appmat.calendar.core.schedule.repositories

import java.time.OffsetDateTime
import org.appmat.calendar.core.common.exception.AccessException
import org.appmat.calendar.core.meeting_template.dao.MeetingTemplateDAO
import org.appmat.calendar.core.meeting_template.repositories.MeetingTemplateTable
import org.appmat.calendar.core.plugins.serialization.DurationIsoSerializer
import org.appmat.calendar.core.schedule.data.ScheduledSlot
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select

class ScheduledMeetingRepository {
    fun scheduledSlots(start: OffsetDateTime, days: Int, userId: String): List<ScheduledSlot> {
        return ScheduledMeetingDAO.wrapRows(
            ScheduledMeetingTable.innerJoin(MeetingTemplateTable).select {
                ScheduledMeetingTable.startDateTime greaterEq start and
                        (ScheduledMeetingTable.startDateTime less start.plusDays(days.toLong())) and
                        (ScheduledMeetingTable.participantId eq userId or (MeetingTemplateTable.userId eq userId))
            }
        ).map { it.toData() }
    }

    fun create(scheduledSlot: ScheduledSlot): ScheduledSlot {
        return ScheduledMeetingDAO.new {
            meetingTemplate = MeetingTemplateDAO[scheduledSlot.meetingTemplateId]
            participantId = scheduledSlot.participantId
            startDateTime = scheduledSlot.dateTimeInterval.startDateTime
            duration = DurationIsoSerializer.serialize(scheduledSlot.dateTimeInterval.duration)
        }.toData()
    }

    fun delete(slotId: Long, userId: String) {
        val scheduledMeeting = ScheduledMeetingDAO[slotId]
        if (scheduledMeeting.participantId != userId && scheduledMeeting.meetingTemplate.userId != userId)
            throw AccessException(id = slotId, userId = userId)
        scheduledMeeting.delete()
    }

    fun get(slotId: Long) = ScheduledMeetingDAO[slotId].toData()

    fun getByTemplateId(templateId: Long, start: OffsetDateTime, days: Int): List<ScheduledSlot> {
        return ScheduledMeetingDAO
            .find {
                ScheduledMeetingTable.meetingTemplate eq templateId and
                        (ScheduledMeetingTable.startDateTime greaterEq start) and
                        (ScheduledMeetingTable.startDateTime less start.plusDays(days.toLong()))
            }
            .map { it.toData() }
    }

    fun getByUserId(userId: String, start: OffsetDateTime, days: Int): List<ScheduledSlot> {
        return ScheduledMeetingDAO
            .find {
                ScheduledMeetingTable.participantId eq userId and
                        (ScheduledMeetingTable.startDateTime greaterEq start) and
                        (ScheduledMeetingTable.startDateTime less start.plusDays(days.toLong()))
            }
            .map { it.toData() }
    }
}
