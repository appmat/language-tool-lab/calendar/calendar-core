package org.appmat.calendar.core.meeting_template.dto

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Список шаблонов
 *
 * @param list
 */
@Serializable
data class MeetingTemplates(

    @SerialName(value = "list") @Required val list: List<MeetingTemplate>

)

