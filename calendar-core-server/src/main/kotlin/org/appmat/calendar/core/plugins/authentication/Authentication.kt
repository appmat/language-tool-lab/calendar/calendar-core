package org.appmat.calendar.core.plugins.authentication

import com.auth0.jwk.JwkProviderBuilder
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.JWTCredential
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.auth.jwt.jwt
import java.net.URL
import java.util.concurrent.TimeUnit
import org.appmat.calendar.core.common.exception.JwtAuthenticationException

fun Application.configureAuthentication() {
    install(Authentication) {
        val issuer = environment.config.property("jwt.issuer").getString()
        val jwksUri = environment.config.property("jwt.jwks_uri").getString()

        val jwkProvider = JwkProviderBuilder(URL(jwksUri))
            .cached(10, 24, TimeUnit.HOURS)
            .rateLimited(10, 1, TimeUnit.MINUTES)
            .build()

        jwt("auth-jwt") {
            verifier(jwkProvider, issuer) {
                acceptLeeway(3)
            }
            validate { credential ->
                log.debug("credential=${credential.payload.subject}")
                if (credential.hasSubject() && credential.hasAppropriateIssuer(issuer)) {
                    JWTPrincipal(credential.payload)
                } else {
                    null
                }
            }
            challenge { _, _ ->
                log.debug("Token is not valid or has expired")
                throw JwtAuthenticationException()
            }
        }
    }
}

private fun JWTCredential.hasSubject() = this.payload.subject != null

private fun JWTCredential.hasAppropriateIssuer(issuer: String) = this.payload.issuer == issuer
