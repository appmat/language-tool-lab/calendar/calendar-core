package org.appmat.calendar.core.meeting_template.converters

import org.appmat.calendar.core.meeting_template.dto.Interval as IntervalDto
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplate as MeetingTemplateDto
import java.util.UUID
import kotlin.time.Duration.Companion.minutes
import org.appmat.calendar.core.common.data.WeekdayTimeInterval
import org.appmat.calendar.core.common.data.MeetingTemplate
import org.appmat.calendar.core.common.data.Weekday
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplateSaveRequest


fun MeetingTemplateSaveRequest.toData(userId: String) = MeetingTemplate(
    id = -1,
    userId = userId,
    title = title,
    description = description,
    place = place,
    duration = durationMin.minutes,
    repeatEveryNthWeek = repeatEveryNthWeek,
    startDay = startDate,
    link = link,
    weekdayTimeIntervals = intervals.map { it.toData() }
)

fun IntervalDto.toData() = WeekdayTimeInterval(
    timeInterval = timeInterval,
    weekday = weekday.toData(),
)

fun IntervalDto.Weekday.toData() = Weekday.valueOf(this.value)

fun Weekday.toDto() = IntervalDto.Weekday.valueOf(this.value)

fun MeetingTemplate.toDto() = MeetingTemplateDto(
    id = this.id,
    title = this.title,
    userId = try {
        UUID.fromString(this.userId)
    } catch (ex: IllegalArgumentException) {
        null
    },
    description = this.description,
    place = this.place,
    durationMin = this.duration.inWholeMinutes.toInt(),
    repeatEveryNthWeek = this.repeatEveryNthWeek,
    startDate = this.startDay,
    link = this.link ?: this.id.toString(),
    intervals = this.weekdayTimeIntervals.map { it.toDto() }
)

fun WeekdayTimeInterval.toDto() = IntervalDto(
    timeInterval = timeInterval,
    weekday = this.weekday.toDto()
)
