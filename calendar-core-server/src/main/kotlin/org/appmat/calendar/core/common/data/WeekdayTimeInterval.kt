package org.appmat.calendar.core.common.data

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.ZoneOffset
import org.appmat.calendar.core.common.data.TimeInterval.Companion.merged

data class WeekdayTimeInterval(
    val timeInterval: TimeInterval,
    val weekday: Weekday,
) {
    companion object {
        fun DayOfWeek.toData() = Weekday.values()[this.ordinal]
        val monday: LocalDate = LocalDate.ofEpochDay(4)

        fun List<WeekdayTimeInterval>.merged(): List<WeekdayTimeInterval> {
            val result = mutableListOf<WeekdayTimeInterval>()

            val intervalsByWeekday = this.groupBy { it.weekday }
            for (weekday in Weekday.values()) {
                val mergedTimeIntervals = (intervalsByWeekday[weekday] ?: emptyList()).map { it.timeInterval }.merged()
                result.addAll(mergedTimeIntervals.map { WeekdayTimeInterval(timeInterval = it, weekday = weekday) })
            }

            return result
        }
    }

    constructor(timeInterval: TimeInterval, dayOfWeek: DayOfWeek) : this(
        timeInterval = timeInterval,
        weekday = dayOfWeek.toData()
    )

    fun withOffsetSameInstant(offset: ZoneOffset): WeekdayTimeInterval {
        val date = monday.plusDays(weekday.ordinal.toLong())
        val dateTime = timeInterval.startTime.atDate(date)
        val shifted = dateTime.withOffsetSameInstant(offset)
        return WeekdayTimeInterval(
            timeInterval = TimeInterval(
                startTime = shifted.toOffsetTime(),
                duration = timeInterval.duration
            ),
            dayOfWeek = shifted.dayOfWeek
        )
    }
}
