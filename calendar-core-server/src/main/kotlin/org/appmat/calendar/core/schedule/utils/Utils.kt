package org.appmat.calendar.core.schedule.utils

import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneOffset

object Utils {
    fun LocalDate.atStartOfDayWithOffset(offset: ZoneOffset): OffsetDateTime = this.atStartOfDay().atOffset(offset)
}
