package org.appmat.calendar.core.schedule.controllers

import io.ktor.locations.Location


@Location("/api")
class Api {
    @Location("/schedule")
    class ScheduleApi(val parent: Api) {
        @Location("/{templateId}")
        class Meeting(val templateId: Long, val parent: ScheduleApi)
    }

    @Location("/slot")
    class SlotApi(val parent: Api) {
        @Location("/{id}")
        class Slot(val id: Long, val parent: SlotApi)

        @Location("/by-user")
        class ByUserApi(val parent: SlotApi)

        @Location("/by-template")
        class ByTemplateId(val parent: SlotApi) {
            @Location("/{id}")
            class Template(val id: Long, val parent: ByTemplateId)
        }
    }
}

