package org.appmat.calendar.core.common.data

import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.time.Duration
import kotlin.time.toJavaDuration
import kotlinx.serialization.ExperimentalSerializationApi
import org.appmat.calendar.core.common.util.Util.duration
import org.appmat.calendar.core.common.util.Util.max
import org.appmat.calendar.core.plugins.serialization.DateTimeIntervalSerializer

@OptIn(ExperimentalSerializationApi::class)
@kotlinx.serialization.Serializable(DateTimeIntervalSerializer::class)
data class DateTimeInterval(
    val startDateTime: OffsetDateTime,
    val duration: Duration,
) {
    companion object {
        fun of(startDateTime: OffsetDateTime, endDateTime: OffsetDateTime) = DateTimeInterval(
            startDateTime = startDateTime,
            duration = duration(startDateTime, endDateTime)
        )

        fun List<DateTimeInterval>.merged(): List<DateTimeInterval> {
            if (this.isEmpty()) return this

            val result = mutableListOf<DateTimeInterval>()

            val sorted = this.sortedBy { it.startDateTime }

            val firstTimeInterval = sorted.first()
            var start = firstTimeInterval.startDateTime
            var end = firstTimeInterval.endDateTime()

            for (timeInterval in sorted) {
                if (timeInterval.startDateTime <= end) {
                    end = max(end, timeInterval.endDateTime())
                } else {
                    result.add(of(startDateTime = start, endDateTime = end))
                    start = timeInterval.startDateTime
                    end = timeInterval.endDateTime()
                }
            }

            result.add(of(startDateTime = start, endDateTime = end))

            return result
        }
    }

    fun endDateTime(): OffsetDateTime = startDateTime.plus(duration.toJavaDuration())

    fun withOffsetSameInstant(offset: ZoneOffset) = copy(startDateTime = startDateTime.withOffsetSameInstant(offset))
}
