package org.appmat.calendar.core.meeting_template.repositories

import org.appmat.calendar.core.common.data.Weekday
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.ReferenceOption

object IntervalTable : LongIdTable("interval") {
    val meetingTemplate = reference(
        "meeting_template_id",
        MeetingTemplateTable,
        onDelete = ReferenceOption.CASCADE,
        onUpdate = ReferenceOption.CASCADE,
    )
    val startTime = text("start_time")
    val duration = text("duration")
    val weekday = enumerationByName("week_day", length = 9, Weekday::class)
}
