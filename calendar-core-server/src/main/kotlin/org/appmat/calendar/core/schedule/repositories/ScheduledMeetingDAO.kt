package org.appmat.calendar.core.schedule.repositories

import org.appmat.calendar.core.common.data.DateTimeInterval
import org.appmat.calendar.core.meeting_template.dao.MeetingTemplateDAO
import org.appmat.calendar.core.plugins.serialization.DurationIsoSerializer
import org.appmat.calendar.core.schedule.data.ScheduledSlot
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class ScheduledMeetingDAO(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<ScheduledMeetingDAO>(ScheduledMeetingTable)

    var meetingTemplate by MeetingTemplateDAO referencedOn ScheduledMeetingTable.meetingTemplate
    var participantId by ScheduledMeetingTable.participantId
    var startDateTime by ScheduledMeetingTable.startDateTime
    var duration by ScheduledMeetingTable.duration

    fun toData() = ScheduledSlot(
        id = id.value,
        dateTimeInterval = DateTimeInterval(
            startDateTime = startDateTime,
            duration = DurationIsoSerializer.deserialize(duration)
        ),
        participantId = participantId,
        meetingTemplateId = meetingTemplate.id.value,
    )
}
