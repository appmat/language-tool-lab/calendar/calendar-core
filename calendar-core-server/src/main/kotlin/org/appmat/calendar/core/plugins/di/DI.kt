package org.appmat.calendar.core.plugins.di

import io.ktor.application.Application
import io.ktor.application.install
import org.koin.core.module.Module
import org.koin.ktor.ext.Koin
import org.koin.logger.slf4jLogger

fun Application.configureDI(koinModules: List<Module>) {
    install(Koin) {
        slf4jLogger()
        modules(koinModules)
    }
}
