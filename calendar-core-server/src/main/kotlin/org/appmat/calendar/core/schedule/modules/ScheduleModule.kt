package org.appmat.calendar.core.schedule.modules

import org.appmat.calendar.core.meeting_template.repositories.MeetingTemplateRepository
import org.appmat.calendar.core.plugins.routing.KtorController
import org.appmat.calendar.core.schedule.controllers.ScheduleController
import org.appmat.calendar.core.schedule.repositories.ScheduledMeetingRepository
import org.appmat.calendar.core.schedule.services.MeetingTemplateService
import org.appmat.calendar.core.schedule.services.ScheduleService
import org.koin.dsl.bind
import org.koin.dsl.module

val scheduleModule = module {
    single { MeetingTemplateRepository() }
    single { ScheduledMeetingRepository() }
    single { org.appmat.calendar.core.meeting_template.services.MeetingTemplateService(get()) }
    single { MeetingTemplateService(get()) }
    single { ScheduleService(get(), get()) }
    single { ScheduleController(get()) } bind KtorController::class
}
