package org.appmat.calendar.core.schedule.repositories

import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlinx.datetime.toInstant
import kotlinx.datetime.toJavaInstant
import org.appmat.calendar.core.meeting_template.repositories.MeetingTemplateTable
import org.appmat.calendar.core.plugins.serialization.OffsetDateTimeDataBaseSerializer
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.IDateColumnType
import org.jetbrains.exposed.sql.ReferenceOption

object ScheduledMeetingTable : LongIdTable("scheduled_meeting") {
    val meetingTemplate = reference(
        "meeting_template_id",
        MeetingTemplateTable,
        onDelete = ReferenceOption.SET_NULL,
        onUpdate = ReferenceOption.CASCADE,
    )
    val participantId = text("participant_id")
    val startDateTime: Column<OffsetDateTime> =
        registerColumn("start_date_time", JavaOffsetDateTimeColumnType.INSTANCE)
    val duration = text("duration")
}

class JavaOffsetDateTimeColumnType : ColumnType(), IDateColumnType {
    override val hasTimePart: Boolean = true
    override fun sqlType(): String = "TIMESTAMP WITH TIME ZONE"

    override fun nonNullValueToString(value: Any) = when (value) {
        is String -> value
        is OffsetDateTime -> OffsetDateTimeDataBaseSerializer.serialize(value)
        else -> error("Unexpected value: $value of ${value::class.qualifiedName}")
    }

    override fun valueFromDB(value: Any): OffsetDateTime = when (value) {
        is OffsetDateTime -> value
        is java.sql.Timestamp -> value.toInstant().atOffset(ZoneOffset.UTC)
        is String -> value.toInstant().toJavaInstant().atOffset(ZoneOffset.UTC)
        else -> valueFromDB(value.toString())
    }

    companion object {
        internal val INSTANCE = JavaOffsetDateTimeColumnType()
    }
}
