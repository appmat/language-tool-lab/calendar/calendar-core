package org.appmat.calendar.core.greeting

class GreetingServiceImpl : GreetingService {
    override fun greet() = "Hello World!"
}
