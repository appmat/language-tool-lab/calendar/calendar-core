@file:UseSerializers(LocalDateSerializer::class)

package org.appmat.calendar.core.meeting_template.dto

import java.time.LocalDate
import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.appmat.calendar.core.plugins.serialization.LocalDateSerializer

/**
 * Объект запроса на создание шаблона встречи
 *
 * @param title Название встречи
 * @param durationMin Длительность встречи в минутах
 * @param repeatEveryNthWeek Повторяемость встречи. Каждые n недель
 * @param startDate День, начиная с которого начинать проводить встречи
 * @param intervals
 * @param description Подробное описание встречи
 * @param place Место встречи
 * @param link Кастомная ссылка для записи на встречу, задаваемая пользователем. По умолчанию будет задана идентификатором шаблона.
 */
@Serializable
data class MeetingTemplateSaveRequest(

    /* Название встречи */
    @SerialName(value = "title") @Required val title: String,

    /* Длительность встречи в минутах */
    @SerialName(value = "durationMin") @Required val durationMin: Int,

    /* Повторяемость встречи. Каждые n недель */
    @SerialName(value = "repeatEveryNthWeek") @Required val repeatEveryNthWeek: Int,

    /* День, начиная с которого начинать проводить встречи */
    @SerialName(value = "startDate") @Required val startDate: LocalDate,

    @SerialName(value = "intervals") @Required val intervals: List<Interval>,

    /* Подробное описание встречи */
    @SerialName(value = "description") val description: String? = null,

    /* Место встречи */
    @SerialName(value = "place") val place: String? = null,

    /* Кастомная ссылка для записи на встречу, задаваемая пользователем. По умолчанию будет задана идентификатором шаблона. */
    @SerialName(value = "link") val link: String? = null

)

