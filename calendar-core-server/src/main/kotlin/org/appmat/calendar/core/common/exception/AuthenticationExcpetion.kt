package org.appmat.calendar.core.common.exception

class JwtAuthenticationException: RuntimeException("Token is not valid or has expired")
