package org.appmat.calendar.core.meeting_template.dao

import kotlin.time.Duration.Companion.minutes
import org.appmat.calendar.core.common.data.MeetingTemplate
import org.appmat.calendar.core.meeting_template.repositories.IntervalTable
import org.appmat.calendar.core.meeting_template.repositories.MeetingTemplateTable
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class MeetingTemplateDAO(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<MeetingTemplateDAO>(MeetingTemplateTable)

    var userId by MeetingTemplateTable.userId
    var title by MeetingTemplateTable.title
    var description by MeetingTemplateTable.description
    var place by MeetingTemplateTable.place
    var durationMin by MeetingTemplateTable.durationMin
    var frequency by MeetingTemplateTable.frequency
    var startWeek by MeetingTemplateTable.startWeek
    var link by MeetingTemplateTable.link

    val intervals by IntervalDAO referrersOn IntervalTable.meetingTemplate

    fun toData() = MeetingTemplate(
        id = id.value,
        userId = userId,
        title = title,
        description = description,
        place = place,
        duration = durationMin.minutes,
        repeatEveryNthWeek = frequency,
        startDay = startWeek,
        link = link,
        weekdayTimeIntervals = intervals.map { it.toData() }
    )
}
