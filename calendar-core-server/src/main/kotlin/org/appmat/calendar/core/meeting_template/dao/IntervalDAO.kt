package org.appmat.calendar.core.meeting_template.dao

import org.appmat.calendar.core.common.data.WeekdayTimeInterval
import org.appmat.calendar.core.common.data.TimeInterval
import org.appmat.calendar.core.meeting_template.repositories.IntervalTable
import org.appmat.calendar.core.plugins.serialization.DurationIsoSerializer
import org.appmat.calendar.core.plugins.serialization.OffsetTimeIsoSerializer
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class IntervalDAO(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<IntervalDAO>(IntervalTable)

    var meetingTemplate by MeetingTemplateDAO referencedOn IntervalTable.meetingTemplate
    var startTime by IntervalTable.startTime
    var duration by IntervalTable.duration
    var weekday by IntervalTable.weekday

    fun toData() = WeekdayTimeInterval(
        timeInterval = TimeInterval(
            startTime = OffsetTimeIsoSerializer.deserialize(startTime),
            duration = DurationIsoSerializer.deserialize(duration)
        ),
        weekday = weekday,
    )
}
