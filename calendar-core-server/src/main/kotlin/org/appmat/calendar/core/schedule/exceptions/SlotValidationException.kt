package org.appmat.calendar.core.schedule.exceptions

import org.appmat.calendar.core.common.exception.ValidationException

class SlotValidationException(message: String) : ValidationException(message)
