package org.appmat.calendar.core.user

data class User(
    val id: Long,
    val email: String,
    val name: String,
    val surname: String,
    val login: String,
    val password: String,
    val photo: String,
) {
    constructor(email: String, password: String) :
            this(0, email, "", "", "", password, "")

    // only public info
    constructor(name: String, surname: String, login: String, photo: String) :
            this(0, "", name, surname, login, "", photo)

}
