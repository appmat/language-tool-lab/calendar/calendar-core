package org.appmat.calendar.core.meeting_template.controllers

import io.ktor.locations.Location


@Location("/api")
class Api {
    @Location("/meeting-template")
    class MeetingTemplateApi(val parent: Api) {
        @Location("/{id}")
        class MeetingTemplate(val id: Long, val parent: MeetingTemplateApi)
    }
}
