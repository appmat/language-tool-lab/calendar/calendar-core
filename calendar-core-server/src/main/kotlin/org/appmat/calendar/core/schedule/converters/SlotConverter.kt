package org.appmat.calendar.core.schedule.converters

import org.appmat.calendar.core.schedule.dto.Slot as SlotDto
import java.time.LocalDate
import org.appmat.calendar.core.schedule.data.Slot
import org.appmat.calendar.core.schedule.dto.Slots

object SlotConverter {
    fun Slot.toDto() = SlotDto(
        dateTimeInterval = dateTimeInterval
    )

    fun List<Slot>.toDto(templateId: Long, startDate: LocalDate, days: Int) = Slots(
        templateId = templateId,
        start = startDate,
        days = days,
        slots = this.map { it.toDto() },
    )
}
