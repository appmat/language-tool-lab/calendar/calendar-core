package org.appmat.calendar.core.schedule.data

import java.time.ZoneOffset
import org.appmat.calendar.core.common.data.DateTimeInterval

data class ScheduledMeeting(
    val dateTimeInterval: DateTimeInterval,
    val participantIds: List<String>,
    val meetingTemplateId: Long,
) {
    fun withOffsetSameInstant(offset: ZoneOffset): ScheduledMeeting {
        return copy(dateTimeInterval = dateTimeInterval.withOffsetSameInstant(offset))
    }
}
