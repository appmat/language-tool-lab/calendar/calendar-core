package org.appmat.calendar.core.plugins.cors

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod

fun Application.configureCORS(additionalHost: List<String>) {
    install(CORS) {
        allowNonSimpleContentTypes = true
        host("calendar.appmat.org", schemes = listOf("https"))
        additionalHost.forEach {
            host(it)
        }
        header(HttpHeaders.ContentType)
        header(HttpHeaders.Authorization)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
    }
}
