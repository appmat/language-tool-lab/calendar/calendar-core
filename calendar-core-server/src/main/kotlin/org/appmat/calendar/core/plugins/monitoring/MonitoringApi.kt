package org.appmat.plugins

import io.ktor.locations.Location

@Location("/metrics")
class Metrics
