package org.appmat.calendar.core.plugins.monitoring

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.locations.get
import io.ktor.metrics.micrometer.MicrometerMetrics
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.routing.routing
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import org.appmat.plugins.Metrics
import org.slf4j.event.Level

fun Application.configureMonitoring() {
    install(CallLogging) {
        level = Level.INFO
        val methodsToIgnore = Regex("^/(metrics|healthz|readyz)")
        filter { call ->
            call.request.path().matches(methodsToIgnore).not()
        }
    }
    val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    install(MicrometerMetrics) {
        registry = appMicrometerRegistry
    }

    routing {
        get<Metrics> {
            call.respond(appMicrometerRegistry.scrape())
        }
    }
}
