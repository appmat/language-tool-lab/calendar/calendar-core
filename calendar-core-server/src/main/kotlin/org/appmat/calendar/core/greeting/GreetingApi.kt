package org.appmat.calendar.core.greeting

import io.ktor.locations.Location

@Location("/api")
class Api {
    @Location("/greeting")
    class Greeting
}
