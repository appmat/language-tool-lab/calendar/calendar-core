package org.appmat.calendar.core.plugins.serialization

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.appmat.calendar.core.common.data.DateTimeInterval


@ExperimentalSerializationApi
@Serializer(forClass = DateTimeInterval::class)
object DateTimeIntervalSerializer : KSerializer<DateTimeInterval> {

    override fun serialize(encoder: Encoder, value: DateTimeInterval) {
        encoder.encodeString(
            OffsetDateTimeIsoSerializer.serialize(value.startDateTime) + "/"
                    + DurationIsoSerializer.serialize(value.duration)
        )
    }

    override fun deserialize(decoder: Decoder): DateTimeInterval {
        val (dateTime, interval) = decoder.decodeString().split('/')

        return DateTimeInterval(
            startDateTime = OffsetDateTimeIsoSerializer.deserialize(dateTime),
            duration = DurationIsoSerializer.deserialize(interval)
        )
    }
}
