package org.appmat.calendar.core.greeting

import io.ktor.application.call
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Routing
import org.appmat.calendar.core.plugins.routing.KtorController

class GreetingController(
    private val greetingService: GreetingService,
) : KtorController {

    override val routing: Routing.() -> Unit = {
        get<Api.Greeting> {
            call.respondText(greetingService.greet())
        }
    }
}
