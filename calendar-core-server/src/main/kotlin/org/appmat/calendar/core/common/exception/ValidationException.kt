package org.appmat.calendar.core.common.exception

open class ValidationException(message: String) : RuntimeException(message)
