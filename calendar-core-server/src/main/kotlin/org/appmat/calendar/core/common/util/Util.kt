package org.appmat.calendar.core.common.util

import io.ktor.application.ApplicationCall
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.auth.principal
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZoneOffset
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds
import org.appmat.calendar.core.common.exception.ValidationException
import org.appmat.calendar.core.plugins.serialization.LocalDateSerializer
import org.appmat.calendar.core.schedule.exceptions.RequiredQueryParamNotFound

object Util {
    fun max(a: OffsetTime, b: OffsetTime): OffsetTime {
        return if (a > b) a else b
    }

    fun max(a: OffsetDateTime, b: OffsetDateTime): OffsetDateTime {
        return if (a > b) a else b
    }

    fun duration(start: OffsetTime, end: OffsetTime): Duration {
        return (end.toEpochSecond(LocalDate.EPOCH) - start.toEpochSecond(LocalDate.EPOCH)).seconds
    }

    fun duration(start: OffsetDateTime, end: OffsetDateTime): Duration {
        return (end.toEpochSecond() - start.toEpochSecond()).seconds
    }

    fun ApplicationCall.getUserId(): String {
        return this.principal<JWTPrincipal>().getUserId()!!
    }

    fun JWTPrincipal?.getUserId(): String? = this?.payload?.subject

    inline fun <reified T : Comparable<T>> ApplicationCall.receiveAndValidateParam(
        name: String,
        default: T? = null,
        maxValue: T? = null,
        minValue: T? = null,
        convert: ((stringValue: String) -> T) = ::parse,
    ): T {
        val value = request.queryParameters[name]?.let(convert) ?: default
        ?: throw RequiredQueryParamNotFound(name)
        maxValue?.let { if (value > maxValue) throw ValidationException("$name should be less or equal $maxValue") }
        minValue?.let { if (value < minValue) throw ValidationException("$name should be greater or equal $minValue") }
        return value
    }

    inline fun <reified T> ApplicationCall.receiveParam(
        name: String,
        default: T? = null,
        convert: ((stringValue: String) -> T) = ::parse,
    ): T {
        return request.queryParameters[name]?.let(convert) ?: default
        ?: throw RequiredQueryParamNotFound(name)
    }

    inline fun <reified T> ApplicationCall.receiveParamOrNull(
        name: String,
        default: T? = null,
        convert: ((stringValue: String) -> T) = ::parse,
    ): T? {
        return request.queryParameters[name]?.let(convert) ?: default
    }

    /**
     * Parse type T from value
     *
     */
    inline fun <reified T> parse(value: String): T {
        return (when (T::class) {
            Int::class -> value.toInt()
            Long::class -> value.toLong()
            ZoneOffset::class -> ZoneOffset.of(value)
            LocalDate::class -> LocalDateSerializer.deserialize(value)
            String::class -> value
            else -> throw IllegalStateException("Unknown Generic Type")
        } as T)
    }
}
