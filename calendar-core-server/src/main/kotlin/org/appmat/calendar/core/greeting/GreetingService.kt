package org.appmat.calendar.core.greeting

interface GreetingService {
    fun greet(): String
}

