@file:UseSerializers(LocalDateSerializer::class)

package org.appmat.calendar.core.schedule.dto

import java.time.LocalDate
import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.appmat.calendar.core.plugins.serialization.LocalDateSerializer

/**
 * Забронированные временные интервалы пользователя
 *
 * @param scheduledSlots
 * @param start First date inclusive in ISO 8601
 * @param days Кол-во дней, для которых подсчитаны слоты
 */
@Serializable
data class ScheduledSlots(

    @SerialName(value = "scheduledSlots") @Required val scheduledSlots: List<ScheduledSlot>,

    /* First date inclusive in ISO 8601 */
    @SerialName(value = "start") @Required val start: LocalDate,

    /* Кол-во дней, для которых подсчитаны слоты */
    @SerialName(value = "days") @Required val days: Int

)
