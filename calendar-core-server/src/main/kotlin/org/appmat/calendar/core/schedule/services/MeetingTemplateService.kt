package org.appmat.calendar.core.schedule.services

import org.appmat.calendar.core.common.data.MeetingTemplate

class MeetingTemplateService(
    private val meetingTemplateService: org.appmat.calendar.core.meeting_template.services.MeetingTemplateService,
) {
    fun get(meetingTemplateId: Long): MeetingTemplate {
        return meetingTemplateService.get(id = meetingTemplateId)
    }
}
