package org.appmat.calendar.core.plugins.routing

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.locations.Locations
import io.ktor.routing.routing
import org.koin.ktor.ext.getKoin

fun Application.configureRouting() {
    install(Locations)

    routing {
        getKoin().getAll<KtorController>()
            /**
             * prevent from registering one controller twice
             */
            .distinct()
            .forEach { it.routing(this) }
    }
}
