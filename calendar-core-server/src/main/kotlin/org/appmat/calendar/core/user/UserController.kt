package org.appmat.calendar.core.user

import io.ktor.application.call
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.routing.Routing
import org.appmat.calendar.core.plugins.routing.KtorController

class UserController(
    private val userService: UserService,
) : KtorController {

    override val routing: Routing.() -> Unit = {
        get<Api.Users.User> {
            call.respond(userService.getUser(it.id))
        }
    }
}
