@file:UseSerializers(TimeIntervalSerializer::class)

package org.appmat.calendar.core.meeting_template.dto

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.appmat.calendar.core.common.data.TimeInterval
import org.appmat.calendar.core.plugins.serialization.TimeIntervalSerializer

/**
 * Интервал доступный для записи
 *
 * @param weekday
 * @param timeInterval Время начала интервала и длительность в формате ISO 8601
 */
@Serializable
data class Interval(

    @SerialName(value = "weekday") @Required val weekday: Weekday,

    /* Время начала интервала и длительность в формате ISO 8601 */
    @SerialName(value = "timeInterval") val timeInterval: TimeInterval,

    ) {

    /**
     *
     *
     * Values: MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
     */
    @Serializable
    enum class Weekday(val value: String) {
        @SerialName(value = "MONDAY") MONDAY("MONDAY"),
        @SerialName(value = "TUESDAY") TUESDAY("TUESDAY"),
        @SerialName(value = "WEDNESDAY") WEDNESDAY("WEDNESDAY"),
        @SerialName(value = "THURSDAY") THURSDAY("THURSDAY"),
        @SerialName(value = "FRIDAY") FRIDAY("FRIDAY"),
        @SerialName(value = "SATURDAY") SATURDAY("SATURDAY"),
        @SerialName(value = "SUNDAY") SUNDAY("SUNDAY");
    }
}

