package org.appmat.calendar.core

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.Application
import io.ktor.server.netty.EngineMain
import org.appmat.calendar.core.greeting.greetingModule
import org.appmat.calendar.core.meeting_template.modules.meetingTemplateModule
import org.appmat.calendar.core.plugins.authentication.configureAuthentication
import org.appmat.calendar.core.plugins.cors.configureCORS
import org.appmat.calendar.core.plugins.di.configureDI
import org.appmat.calendar.core.plugins.healthcheck.configureHealthChecks
import org.appmat.calendar.core.plugins.monitoring.configureMonitoring
import org.appmat.calendar.core.plugins.routing.configureRouting
import org.appmat.calendar.core.plugins.serialization.configureSerialization
import org.appmat.calendar.core.plugins.statuspages.configureExceptionHandler
import org.appmat.calendar.core.schedule.modules.scheduleModule
import org.appmat.calendar.core.user.userModule
import org.jetbrains.exposed.sql.Database
import org.koin.core.module.Module

fun main(args: Array<String>): Unit = EngineMain.main(args)

/**
 * Please note that you can use any other name instead of *module*.
 * Also note that you can have more then one modules in your application.
 * */
@Suppress("unused") // Referenced in application.conf
@JvmOverloads
fun Application.module(
    testing: Boolean = false,
    koinModules: List<Module> = listOf(greetingModule, userModule, meetingTemplateModule, scheduleModule),
) {
    configureAuthentication()
    configureDI(koinModules)
    configureRouting()
    configureMonitoring()
    configureSerialization()
    configureHealthChecks()
    configureExceptionHandler()

    if (!testing) {
        val corsConfig = environment.config.config("ktor.cors")
        configureCORS(corsConfig.property("hosts").getList())

        val hikariDataSource = HikariDataSource(
            HikariConfig().apply {
                val hikariAppConfig = environment.config.config("ktor.hikari")
                username = hikariAppConfig.property("user").getString()
                password = hikariAppConfig.property("password").getString()
                jdbcUrl = hikariAppConfig.property("jdbcUrl").getString()
                maximumPoolSize = hikariAppConfig.property("maxPoolSize").getString().toInt()
                driverClassName = "org.postgresql.Driver"
            }
        )

        Database.connect(hikariDataSource)
    }
}
