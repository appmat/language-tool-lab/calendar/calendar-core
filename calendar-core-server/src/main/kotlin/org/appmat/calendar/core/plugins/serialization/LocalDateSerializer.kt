package org.appmat.calendar.core.plugins.serialization

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder


@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = LocalDate::class)
object LocalDateSerializer : KSerializer<LocalDate> {
    private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    override fun serialize(encoder: Encoder, value: LocalDate) {
        encoder.encodeString(serialize(value))
    }

    override fun deserialize(decoder: Decoder): LocalDate {
        return deserialize(decoder.decodeString())
    }

    fun serialize(value: LocalDate): String {
        return value.format(formatter)
    }

    fun deserialize(string: String): LocalDate {
        return LocalDate.parse(string, formatter)
    }
}
