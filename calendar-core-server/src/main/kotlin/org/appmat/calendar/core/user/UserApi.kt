package org.appmat.calendar.core.user

import io.ktor.locations.Location

@Location("/api")
class Api {
    @Location("/users")
    class Users(val api: Api) {
        @Location("/{id}")
        class User(val users: Users, val id: Long)
    }
}
