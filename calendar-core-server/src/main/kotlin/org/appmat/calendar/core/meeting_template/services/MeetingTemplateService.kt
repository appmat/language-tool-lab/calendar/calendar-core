package org.appmat.calendar.core.meeting_template.services

import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import org.appmat.calendar.core.common.data.MeetingTemplate
import org.appmat.calendar.core.common.data.WeekdayTimeInterval
import org.appmat.calendar.core.meeting_template.exceptions.DurationValidationException
import org.appmat.calendar.core.meeting_template.exceptions.FrequencyValidationException
import org.appmat.calendar.core.meeting_template.repositories.MeetingTemplateRepository
import org.jetbrains.exposed.sql.transactions.transaction

class MeetingTemplateService(
    private val meetingTemplateRepository: MeetingTemplateRepository,
) {
    fun create(meetingTemplate: MeetingTemplate): MeetingTemplate {
        return transaction {
            meetingTemplate.validate()
            return@transaction meetingTemplateRepository.create(
                meetingTemplate.withMergedIntervalsGreaterOrEqualDuration()
            )
        }
    }

    fun findByUserId(
        userId: String,
        limit: Int,
        offset: Long,
    ): List<MeetingTemplate> {
        return transaction {
            return@transaction meetingTemplateRepository.findByUserId(userId = userId, limit = limit, offset = offset)
        }
    }

    fun get(id: Long): MeetingTemplate {
        return transaction {
            return@transaction meetingTemplateRepository.get(id)
        }
    }

    fun update(id: Long, meetingTemplate: MeetingTemplate): MeetingTemplate {
        return transaction {
            meetingTemplate.validate()
            return@transaction meetingTemplateRepository.update(
                id = id,
                meetingTemplate = meetingTemplate.withMergedIntervalsGreaterOrEqualDuration()
            )
        }
    }

    fun delete(id: Long, userId: String) {
        return transaction {
            return@transaction meetingTemplateRepository.delete(id = id, userId = userId)
        }
    }

    private fun MeetingTemplate.withMergedIntervalsGreaterOrEqualDuration() =
        this.withMergedIntervals().withoutIntervalsLessThanDuration()

    private fun MeetingTemplate.withoutIntervalsLessThanDuration(): MeetingTemplate {
        return this.copy(weekdayTimeIntervals = this.weekdayTimeIntervals.removeIntervalsLessThanDuration(this.duration))
    }

    private fun List<WeekdayTimeInterval>.removeIntervalsLessThanDuration(duration: Duration): List<WeekdayTimeInterval> {
        return this.filter {
            val intervalDuration = it.timeInterval.duration
            intervalDuration >= duration
        }
    }

    private fun MeetingTemplate.validate() {
        if (this.duration.isNegative()) throw DurationValidationException("duration should positive")
        if (this.duration > 1.days) throw DurationValidationException("duration should be less than a day")
        if (this.repeatEveryNthWeek <= 0) throw FrequencyValidationException("frequency should be > 0")
    }
}
