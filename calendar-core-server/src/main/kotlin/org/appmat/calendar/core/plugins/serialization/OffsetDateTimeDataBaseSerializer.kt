package org.appmat.calendar.core.plugins.serialization

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE
import java.time.format.DateTimeFormatter.ISO_LOCAL_TIME
import java.time.format.DateTimeFormatterBuilder

object OffsetDateTimeDataBaseSerializer {
    private val formatter: DateTimeFormatter = DateTimeFormatterBuilder()
        .parseCaseInsensitive()
        .append(ISO_LOCAL_DATE)
        .appendLiteral(' ')
        .append(ISO_LOCAL_TIME)
        .parseLenient()
        .appendLiteral(' ')
        .appendOffsetId()
        .parseStrict()
        .toFormatter()

    fun deserialize(string: String): OffsetDateTime {
        return OffsetDateTime.parse(string, formatter)
    }

    fun serialize(value: OffsetDateTime): String {
        return value.format(formatter)
    }
}
