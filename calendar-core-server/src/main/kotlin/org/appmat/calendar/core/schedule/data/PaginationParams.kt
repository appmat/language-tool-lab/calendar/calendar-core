package org.appmat.calendar.core.schedule.data

import java.time.LocalDate
import java.time.ZoneOffset

data class PaginationParams(
    val days: Int,
    val startDate: LocalDate,
    val offset: ZoneOffset,
) {
    companion object {
        const val DAYS_PARAM = "days"
        const val START_DATE_PARAM = "start"
        const val ZONE_OFFSET_PARAM = "zoneOffset"
    }
}
