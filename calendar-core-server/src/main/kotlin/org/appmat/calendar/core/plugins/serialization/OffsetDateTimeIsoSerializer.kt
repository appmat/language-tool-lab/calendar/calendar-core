package org.appmat.calendar.core.plugins.serialization

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter


object OffsetDateTimeIsoSerializer {
    private val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    fun deserialize(string: String): OffsetDateTime {
        return OffsetDateTime.parse(string, formatter)
    }

    fun serialize(value: OffsetDateTime): String {
        return value.format(formatter)
    }
}
