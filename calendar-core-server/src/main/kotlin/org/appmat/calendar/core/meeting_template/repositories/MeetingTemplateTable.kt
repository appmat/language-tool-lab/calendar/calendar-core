package org.appmat.calendar.core.meeting_template.repositories

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.`java-time`.date

object MeetingTemplateTable : LongIdTable("meeting_template") {
    val userId = varchar("user_id", 255)
    val title = varchar("title", 255)
    val description = varchar("description", 1000).nullable()
    val place = varchar("place", 1000).nullable()
    val durationMin = integer("duration_min")
    val frequency = integer("repeat_every_nth_week")
    val startWeek = date("start_day")
    val link = varchar("link", 1000).nullable()
}
