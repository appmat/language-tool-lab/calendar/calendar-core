package org.appmat.calendar.core.plugins.statuspages

import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.features.ParameterConversionException
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText
import org.appmat.calendar.core.common.exception.AccessException
import org.appmat.calendar.core.common.exception.JwtAuthenticationException
import org.appmat.calendar.core.common.exception.ValidationException
import org.appmat.calendar.core.schedule.exceptions.RequiredQueryParamNotFound
import org.jetbrains.exposed.dao.exceptions.EntityNotFoundException

fun Application.configureExceptionHandler() {
    install(StatusPages) {
        exception<Throwable> { cause ->
            log.error("Exception happened $cause")
            when (cause) {
                is ParameterConversionException, is RequiredQueryParamNotFound, is ValidationException -> call.respondError(
                    status = HttpStatusCode.BadRequest, cause = cause // 400
                )
                is JwtAuthenticationException -> call.respondError(
                    status = HttpStatusCode.Unauthorized, cause = cause // 401
                )
                is AccessException -> call.respondError(
                    status = HttpStatusCode.Forbidden, cause = cause // 403
                )
                is EntityNotFoundException -> call.respondError(
                    status = HttpStatusCode.NotFound, cause = cause // 404
                )
                else -> call.respondError(
                    status = HttpStatusCode.InternalServerError, cause = cause // 500
                )
            }
        }
    }
}

private suspend fun ApplicationCall.respondError(status: HttpStatusCode, cause: Throwable? = null) =
    this.respondText(text = "${status.value}: $cause", status = status)
