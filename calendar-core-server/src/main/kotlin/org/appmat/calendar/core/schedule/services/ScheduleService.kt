package org.appmat.calendar.core.schedule.services

import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import org.appmat.calendar.core.common.data.DateTimeInterval
import org.appmat.calendar.core.common.data.DateTimeInterval.Companion.merged
import org.appmat.calendar.core.common.data.WeekdayTimeInterval.Companion.toData
import org.appmat.calendar.core.schedule.data.ScheduledSlot
import org.appmat.calendar.core.schedule.data.Slot
import org.appmat.calendar.core.schedule.exceptions.SlotValidationException
import org.appmat.calendar.core.schedule.repositories.ScheduledMeetingRepository
import org.appmat.calendar.core.schedule.utils.Utils.atStartOfDayWithOffset
import org.jetbrains.exposed.sql.transactions.transaction

class ScheduleService(
    private val scheduledMeetingRepository: ScheduledMeetingRepository,
    private val meetingTemplateService: MeetingTemplateService,
) {
    fun availableSlots(
        meetingTemplateId: Long,
        startDate: LocalDate,
        days: Int,
        userId: String?,
        offset: ZoneOffset
    ): List<Slot> {
        val userMeetings = userId?.let {
            transaction {
                scheduledMeetingRepository.scheduledSlots(
                    start = startDate.atStartOfDayWithOffset(offset),
                    days = days,
                    userId = userId
                )
            }
        } ?: emptyList()

        val meetingTemplate = meetingTemplateService.get(meetingTemplateId = meetingTemplateId)
            .withOffsetSameInstant(offset)

        val ownerMeetings = transaction {
            scheduledMeetingRepository.scheduledSlots(
                start = startDate.atStartOfDayWithOffset(offset),
                days = days,
                userId = meetingTemplate.userId
            )
        }

        val scheduledIntervals =
            (userMeetings + ownerMeetings).map { it.withOffsetSameInstant(offset).dateTimeInterval }.merged()
                .sortedBy { it.startDateTime }

        var weekday = startDate.dayOfWeek.toData()
        val intervalsByWeekday = meetingTemplate.weekdayTimeIntervals.groupBy { it.weekday }

        val datedIntervals = mutableListOf<DateTimeInterval>()
        repeat(days) { dayCount ->
            intervalsByWeekday[weekday]?.let { intervals ->
                datedIntervals.addAll(
                    intervals.map { it.timeInterval.atDate(startDate.plusDays(dayCount.toLong())) }
                )
            }

            weekday = weekday.next()
        }

        val sorted = datedIntervals.sortedBy { it.startDateTime }

        // fill available intervals
        val availableIntervals = mutableListOf<DateTimeInterval>()
        var index = 0
        for (interval in sorted) {
            var start = interval.startDateTime
            var end = interval.endDateTime()

            while (index < scheduledIntervals.size && scheduledIntervals[index].startDateTime < interval.endDateTime() && scheduledIntervals[index].endDateTime() > interval.startDateTime) {
                if (scheduledIntervals[index].startDateTime < interval.startDateTime) {
                    start = scheduledIntervals[index].endDateTime()
                } else {
                    end = scheduledIntervals[index].startDateTime

                    if (end > start)
                        availableIntervals.add(DateTimeInterval.of(startDateTime = start, endDateTime = end))

                    start = scheduledIntervals[index].endDateTime()
                    end = interval.endDateTime()
                }
                index += 1
            }

            if (end > start)
                availableIntervals.add(DateTimeInterval.of(startDateTime = start, endDateTime = end))
        }

        // split available intervals to slots with step
        val step = 15 // minutes
        val result = mutableListOf<Slot>()
        for (interval in availableIntervals) {
            val emptyTime = interval.duration - meetingTemplate.duration
            if (emptyTime.isNegative()) continue
            val availableSlotCount = (emptyTime.inWholeMinutes / step).toInt() + 1
            repeat(availableSlotCount) {
                result.add(
                    Slot(
                        meetingTemplateId = meetingTemplateId,
                        dateTimeInterval = DateTimeInterval(
                            startDateTime = interval.startDateTime.plusMinutes((it * step).toLong()),
                            duration = meetingTemplate.duration
                        )
                    )
                )
            }
        }

        return result
    }

    fun create(scheduledSlot: ScheduledSlot): ScheduledSlot {
        scheduledSlot.validate()
        return transaction { scheduledMeetingRepository.create(scheduledSlot) }
    }

    fun delete(slotId: Long, userId: String) {
        return transaction { scheduledMeetingRepository.delete(slotId = slotId, userId = userId) }
    }

    fun get(slotId: Long): ScheduledSlot {
        return transaction { scheduledMeetingRepository.get(slotId) }
    }

    fun getByTemplateId(templateId: Long, start: OffsetDateTime, days: Int): List<ScheduledSlot> {
        return transaction { scheduledMeetingRepository.getByTemplateId(templateId, start, days) }
    }

    fun getByUserId(userId: String, start: OffsetDateTime, days: Int): List<ScheduledSlot> {
        return transaction { scheduledMeetingRepository.getByUserId(userId, start, days) }
    }

    private fun ScheduledSlot.validate() {
        if (this.toSlot() !in availableSlots(
                meetingTemplateId = meetingTemplateId,
                startDate = dateTimeInterval.startDateTime.toLocalDate(),
                days = 1,
                userId = participantId,
                offset = dateTimeInterval.startDateTime.offset
            )
        ) throw SlotValidationException("Slot is unavailable")
    }
}
