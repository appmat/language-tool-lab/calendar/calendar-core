package org.appmat.calendar.core.plugins.serialization

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.appmat.calendar.core.common.data.TimeInterval

@OptIn(ExperimentalSerializationApi::class)
@Serializer(forClass = TimeInterval::class)
object TimeIntervalSerializer : KSerializer<TimeInterval> {

    override fun serialize(encoder: Encoder, value: TimeInterval) {
        encoder.encodeString(
            OffsetTimeIsoSerializer.serialize(value.startTime) + "/" + DurationIsoSerializer.serialize(value.duration)
        )
    }

    override fun deserialize(decoder: Decoder): TimeInterval {
        val (dateTime, interval) = decoder.decodeString().split('/')

        return TimeInterval(
            startTime = OffsetTimeIsoSerializer.deserialize(dateTime),
            duration = DurationIsoSerializer.deserialize(interval)
        )
    }
}
