package org.appmat.calendar.core.schedule.data

import org.appmat.calendar.core.common.data.DateTimeInterval

data class Slot(
    val meetingTemplateId: Long,
    val dateTimeInterval: DateTimeInterval,
)
