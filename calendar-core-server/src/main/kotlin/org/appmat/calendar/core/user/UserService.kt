package org.appmat.calendar.core.user

interface UserService {
    fun getUser(id: Long): User
}
