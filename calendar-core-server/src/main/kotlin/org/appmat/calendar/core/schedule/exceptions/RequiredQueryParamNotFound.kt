package org.appmat.calendar.core.schedule.exceptions

class RequiredQueryParamNotFound(name: String) : RuntimeException("Required query param not found. Name=$name")
