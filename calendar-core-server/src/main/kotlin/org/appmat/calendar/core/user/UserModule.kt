package org.appmat.calendar.core.user

import org.appmat.calendar.core.plugins.routing.KtorController
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single

val userModule = module {
    single<UserRepository>()
    single<UserService> { UserServiceImpl(get()) }
    single { UserController(get()) } bind KtorController::class
}
