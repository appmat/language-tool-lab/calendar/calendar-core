package org.appmat.calendar.core.user

class UserServiceImpl(
    private val userRepository: UserRepository,
) : UserService {
    override fun getUser(id: Long): User {
        return userRepository.getUser(id)
    }
}
