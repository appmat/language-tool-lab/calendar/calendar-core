@file:UseSerializers(LocalDateSerializer::class, UUIDSerializer::class)

package org.appmat.calendar.core.meeting_template.dto

import java.time.LocalDate
import java.util.UUID
import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.appmat.calendar.core.plugins.serialization.LocalDateSerializer
import org.appmat.calendar.core.plugins.serialization.UUIDSerializer

/**
 * Итоговый шаблон встречи
 *
 * @param id id шаблона
 * @param title Название встречи
 * @param durationMin Длительность встречи в минутах
 * @param repeatEveryNthWeek Период повторения в неделях
 * @param startDate День, начиная с которого начинать проводить встречи
 * @param intervals
 * @param link Ссылка для записи на встречу
 * @param userId id владельца шаблона
 * @param description Подробное описание встречи
 * @param place Место встречи
 */
@Serializable
data class MeetingTemplate(

    /* id шаблона */
    @SerialName(value = "id") @Required val id: Long,

    /* Название встречи */
    @SerialName(value = "title") @Required val title: String,

    /* Длительность встречи в минутах */
    @SerialName(value = "durationMin") @Required val durationMin: Int,

    /* Период повторения в неделях */
    @SerialName(value = "repeatEveryNthWeek") @Required val repeatEveryNthWeek: Int,

    /* День, начиная с которого начинать проводить встречи */
    @SerialName(value = "startDate") @Required val startDate: LocalDate,

    @SerialName(value = "intervals") @Required val intervals: List<Interval>,

    /* Ссылка для записи на встречу */
    @SerialName(value = "link") @Required val link: String,

    /* id владельца шаблона */
    @SerialName(value = "userId") val userId: UUID? = null,

    /* Подробное описание встречи */
    @SerialName(value = "description") val description: String? = null,

    /* Место встречи */
    @SerialName(value = "place") val place: String? = null

)

