package org.appmat.calendar.core.schedule.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.appmat.calendar.core.common.data.DateTimeInterval

/**
 * Доступный для записи временно интервал
 *
 * @param dateTimeInterval Дата и время начала интервала и длительность в формате ISO 8601
 */
@Serializable
data class Slot(

    /* Дата и время начала интервала и длительность в формате ISO 8601 */
    @SerialName(value = "dateTimeInterval") val dateTimeInterval: DateTimeInterval

)

