package org.appmat.calendar.core.greeting

import org.appmat.calendar.core.plugins.routing.KtorController
import org.koin.dsl.bind
import org.koin.dsl.module

val greetingModule = module {
    single<GreetingService> { GreetingServiceImpl() }
    single { GreetingController(get()) } bind KtorController::class
}
