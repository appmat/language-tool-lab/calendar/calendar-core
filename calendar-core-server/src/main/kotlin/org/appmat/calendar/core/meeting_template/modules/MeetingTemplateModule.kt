package org.appmat.calendar.core.meeting_template.modules

import org.appmat.calendar.core.meeting_template.controllers.MeetingTemplateController
import org.appmat.calendar.core.meeting_template.repositories.MeetingTemplateRepository
import org.appmat.calendar.core.meeting_template.services.MeetingTemplateService
import org.appmat.calendar.core.plugins.routing.KtorController
import org.koin.dsl.bind
import org.koin.dsl.module

val meetingTemplateModule = module {
    single { MeetingTemplateRepository() }
    single { MeetingTemplateService(get()) }
    single { MeetingTemplateController(get()) } bind KtorController::class
}
