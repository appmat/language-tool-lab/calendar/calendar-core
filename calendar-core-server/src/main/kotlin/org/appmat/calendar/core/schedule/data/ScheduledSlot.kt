package org.appmat.calendar.core.schedule.data

import java.time.ZoneOffset
import org.appmat.calendar.core.common.data.DateTimeInterval

data class ScheduledSlot(
    val id: Long,
    val dateTimeInterval: DateTimeInterval,
    val participantId: String,
    val meetingTemplateId: Long,
) {

    fun withOffsetSameInstant(offset: ZoneOffset): ScheduledSlot {
        return copy(dateTimeInterval = dateTimeInterval.withOffsetSameInstant(offset))
    }

    fun toSlot() = Slot(dateTimeInterval = dateTimeInterval, meetingTemplateId = meetingTemplateId)
}
