package org.appmat.calendar.core.schedule.dto

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.appmat.calendar.core.common.data.DateTimeInterval

/**
 * Забронированный временной интервал
 * @param id
 * @param dateTimeInterval Дата и время начала интервала и длительность в формате ISO 8601
 * @param participantId Id записвашегося пользователя
 * @param meetingTemplateId id шаблона
 */
@Serializable
data class ScheduledSlot(
    @SerialName(value = "id") val id: Long? = null,

    /* Дата и время начала интервала и длительность в формате ISO 8601 */
    @SerialName(value = "dateTimeInterval") @Required val dateTimeInterval: DateTimeInterval,

    /* Id записвашегося пользователя */
    @SerialName(value = "participantId") val participantId: String? = null,

    /* id шаблона */
    @SerialName(value = "meetingTemplateId") @Required val meetingTemplateId: Long

)

