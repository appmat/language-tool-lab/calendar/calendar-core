package org.appmat.calendar.core.plugins.serialization

import kotlin.time.Duration

object DurationIsoSerializer {
    fun serialize(value: Duration): String {
        return value.toIsoString()
    }

    fun deserialize(string: String): Duration {
        return Duration.parseIsoString(string)
    }
}
