package org.appmat.calendar.core.plugins.routing

import io.ktor.routing.Routing

interface KtorController {
    val routing: Routing.() -> Unit
}
