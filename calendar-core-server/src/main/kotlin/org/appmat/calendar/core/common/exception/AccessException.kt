package org.appmat.calendar.core.common.exception

class AccessException(id: Long, userId: String) :
    RuntimeException("Entity with id=$id is not belong to user with userId=$userId")
