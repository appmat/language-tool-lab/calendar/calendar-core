package org.appmat.calendar.core.schedule.converters

import org.appmat.calendar.core.schedule.dto.ScheduledSlot as ScheduledSlotDto
import java.time.LocalDate
import org.appmat.calendar.core.schedule.data.ScheduledSlot
import org.appmat.calendar.core.schedule.dto.ScheduledSlots

object ScheduledSlotConverter {
    fun ScheduledSlotDto.toData(userId: String, defaultId: Long = -1) = ScheduledSlot(
        id = id ?: defaultId,
        dateTimeInterval = dateTimeInterval,
        participantId = userId,
        meetingTemplateId = meetingTemplateId,
    )

    fun ScheduledSlot.toDto() = ScheduledSlotDto(
        id = id,
        dateTimeInterval = dateTimeInterval,
        participantId = participantId,
        meetingTemplateId = meetingTemplateId,
    )

    fun List<ScheduledSlot>.toDto(startDate: LocalDate, days: Int) =
        ScheduledSlots(scheduledSlots = map { it.toDto() }, start = startDate, days = days)
}
