package org.appmat.calendar.core.meeting_template.exceptions

import org.appmat.calendar.core.common.exception.ValidationException

class FrequencyValidationException(message: String) : ValidationException(message)
