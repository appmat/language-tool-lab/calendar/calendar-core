package org.appmat.calendar.core.plugins.serialization

import java.time.OffsetTime
import java.time.format.DateTimeFormatter


object OffsetTimeIsoSerializer {
    private val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_TIME

    fun deserialize(string: String): OffsetTime {
        return OffsetTime.parse(string.drop(1), formatter)
    }

    fun serialize(value: OffsetTime): String {
        return "T" + value.format(formatter)
    }
}
