package org.appmat.calendar.core.meeting_template.repositories

import org.appmat.calendar.core.common.data.MeetingTemplate
import org.appmat.calendar.core.common.data.WeekdayTimeInterval
import org.appmat.calendar.core.common.exception.AccessException
import org.appmat.calendar.core.meeting_template.dao.IntervalDAO
import org.appmat.calendar.core.meeting_template.dao.MeetingTemplateDAO
import org.appmat.calendar.core.plugins.serialization.DurationIsoSerializer
import org.appmat.calendar.core.plugins.serialization.OffsetTimeIsoSerializer

class MeetingTemplateRepository {
    fun create(meetingTemplate: MeetingTemplate): MeetingTemplate {
        val newMeetingTemplate = MeetingTemplateDAO.new {
            userId = meetingTemplate.userId
            title = meetingTemplate.title
            description = meetingTemplate.description
            place = meetingTemplate.place
            durationMin = meetingTemplate.duration.inWholeMinutes.toInt()
            frequency = meetingTemplate.repeatEveryNthWeek
            startWeek = meetingTemplate.startDay
            link = meetingTemplate.link
        }
        createIntervals(meetingTemplate.weekdayTimeIntervals, newMeetingTemplate)
        return newMeetingTemplate.toData()
    }

    fun get(id: Long): MeetingTemplate {
        return MeetingTemplateDAO[id].toData()
    }

    fun findByUserId(
        userId: String,
        limit: Int,
        offset: Long,
    ): List<MeetingTemplate> {
        return MeetingTemplateDAO.find { MeetingTemplateTable.userId.eq(userId) }
            .limit(n = limit, offset = offset)
            .map { it.toData() }
    }

    fun update(id: Long, meetingTemplate: MeetingTemplate): MeetingTemplate {
        val existing = MeetingTemplateDAO[id]
        if (existing.userId != meetingTemplate.userId)
            throw AccessException(id = id, userId = meetingTemplate.userId)
        existing.title = meetingTemplate.title
        existing.description = meetingTemplate.description
        existing.place = meetingTemplate.place
        existing.durationMin = meetingTemplate.duration.inWholeMinutes.toInt()
        existing.frequency = meetingTemplate.repeatEveryNthWeek
        existing.startWeek = meetingTemplate.startDay
        existing.link = meetingTemplate.link
        existing.intervals.forEach { it.delete() }
        createIntervals(meetingTemplate.weekdayTimeIntervals, existing)
        return existing.toData()
    }

    private fun createIntervals(weekdayTimeIntervals: List<WeekdayTimeInterval>, owner: MeetingTemplateDAO) {
        weekdayTimeIntervals.forEach {
            IntervalDAO.new {
                startTime = OffsetTimeIsoSerializer.serialize(it.timeInterval.startTime)
                duration = DurationIsoSerializer.serialize(it.timeInterval.duration)
                weekday = it.weekday
                meetingTemplate = owner
            }
        }
    }

    fun delete(id: Long, userId: String) {
        val meetingTemplate = MeetingTemplateDAO[id]
        if (meetingTemplate.userId != userId)
            throw AccessException(id = id, userId = userId)
        meetingTemplate.delete()
    }
}
