package org.appmat.calendar.core.meeting_template.controllers

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.locations.delete
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.locations.put
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import org.appmat.calendar.core.common.util.Util.getUserId
import org.appmat.calendar.core.common.util.Util.receiveAndValidateParam
import org.appmat.calendar.core.meeting_template.converters.toData
import org.appmat.calendar.core.meeting_template.converters.toDto
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplateSaveRequest
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplates
import org.appmat.calendar.core.meeting_template.services.MeetingTemplateService
import org.appmat.calendar.core.plugins.routing.KtorController

class MeetingTemplateController(
    private val meetingTemplateService: MeetingTemplateService,
) : KtorController {
    override val routing: Routing.() -> Unit = {
        authenticate("auth-jwt") {
            // save meeting template
            post<Api.MeetingTemplateApi> {
                val template = call.receive<MeetingTemplateSaveRequest>().toData(userId = call.getUserId())
                val savedTemplate = meetingTemplateService.create(template)
                call.respond(HttpStatusCode.Created, savedTemplate.toDto())
            }

            // get all user's meeting templates
            get<Api.MeetingTemplateApi> {
                val (limit, offset) = call.receiveAndValidatePaginationParams()
                val templates = meetingTemplateService.findByUserId(userId = call.getUserId(), limit = limit, offset = offset)
                call.respond(HttpStatusCode.OK, MeetingTemplates(templates.map { it.toDto() }))
            }

            // update meeting template
            put<Api.MeetingTemplateApi.MeetingTemplate> {
                val template = call.receive<MeetingTemplateSaveRequest>().toData(call.getUserId())
                val updated = meetingTemplateService.update(it.id, template)
                call.respond(HttpStatusCode.OK, updated.toDto())
            }

            // delete meeting template
            delete<Api.MeetingTemplateApi.MeetingTemplate> {
                meetingTemplateService.delete(it.id, userId = call.getUserId())
                call.respond(HttpStatusCode.OK)
            }
        }

        // get meeting template by id
        get<Api.MeetingTemplateApi.MeetingTemplate> {
            val template = meetingTemplateService.get(it.id)
            call.respond(HttpStatusCode.OK, template.toDto())
        }
    }

    // limit and offset
    private fun ApplicationCall.receiveAndValidatePaginationParams(): Pair<Int, Long> {
        val limit = this.receiveAndValidateParam<Int>(name = "limit", default = 20, maxValue = 100, minValue = 1)
        val offset = this.receiveAndValidateParam<Long>(name = "offset", default = 0, minValue = 0)
        return limit to offset
    }
}
