package org.appmat.calendar.core.common.data

import java.time.LocalDate
import java.time.ZoneOffset
import kotlin.time.Duration
import org.appmat.calendar.core.common.data.WeekdayTimeInterval.Companion.merged

data class MeetingTemplate(
    val id: Long,
    val userId: String,
    val title: String,
    val description: String?,
    val place: String?,
    val duration: Duration,
    val repeatEveryNthWeek: Int,
    val startDay: LocalDate,
    val link: String?,
    val weekdayTimeIntervals: List<WeekdayTimeInterval>,
) {

    fun withMergedIntervals(): MeetingTemplate {
        return this.copy(weekdayTimeIntervals = weekdayTimeIntervals.merged())
    }

    fun withOffsetSameInstant(offset: ZoneOffset): MeetingTemplate {
        return copy(weekdayTimeIntervals = weekdayTimeIntervals.map { it.withOffsetSameInstant(offset) })
    }


}
