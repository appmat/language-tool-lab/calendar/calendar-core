package org.appmat.calendar.core.schedule.controllers

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.locations.delete
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import java.time.LocalDate
import java.time.ZoneOffset
import org.appmat.calendar.core.common.util.Util.getUserId
import org.appmat.calendar.core.common.util.Util.receiveAndValidateParam
import org.appmat.calendar.core.common.util.Util.receiveParam
import org.appmat.calendar.core.plugins.routing.KtorController
import org.appmat.calendar.core.schedule.converters.ScheduledSlotConverter.toData
import org.appmat.calendar.core.schedule.converters.ScheduledSlotConverter.toDto
import org.appmat.calendar.core.schedule.converters.SlotConverter.toDto
import org.appmat.calendar.core.schedule.data.PaginationParams
import org.appmat.calendar.core.schedule.data.PaginationParams.Companion.DAYS_PARAM
import org.appmat.calendar.core.schedule.data.PaginationParams.Companion.START_DATE_PARAM
import org.appmat.calendar.core.schedule.data.PaginationParams.Companion.ZONE_OFFSET_PARAM
import org.appmat.calendar.core.schedule.dto.ScheduledSlot
import org.appmat.calendar.core.schedule.services.ScheduleService
import org.appmat.calendar.core.schedule.utils.Utils.atStartOfDayWithOffset

class ScheduleController(
    private val scheduleService: ScheduleService,
) : KtorController {

    override val routing: Routing.() -> Unit = {
        authenticate("auth-jwt") {
            // Возвращает доступные для заданного userId промежутки времени для записи на мероприятие.
            // Учитывает занятость заданного пользователя и владельца шаблона.
            get<Api.ScheduleApi.Meeting> { meetingTemplate ->
                val (days, startDate, offset) = call.receiveAndValidatePaginationParams()
                val meetingTemplateId = meetingTemplate.templateId
                val slots = scheduleService.availableSlots(
                    meetingTemplateId = meetingTemplateId,
                    startDate = startDate,
                    days = days,
                    userId = call.getUserId(),
                    offset = offset,
                )
                call.respond(
                    HttpStatusCode.OK,
                    slots.toDto(templateId = meetingTemplateId, startDate = startDate, days = days)
                )
            }

            // Сохраняет слот
            post<Api.ScheduleApi> {
                val scheduledSlot = call.receive<ScheduledSlot>().toData(call.getUserId())
                val created = scheduleService.create(scheduledSlot)
                call.respond(HttpStatusCode.Created, created.toDto())
            }

            // Удаляет заброннированный слот по его id
            delete<Api.SlotApi.Slot> {
                scheduleService.delete(slotId = it.id, userId = call.getUserId())
                call.respond(HttpStatusCode.OK)
            }

            // Возвращает список заброннированных слотов пользователя
            get<Api.SlotApi.ByUserApi> {
                val (days, startDate, offset) = call.receiveAndValidatePaginationParams()
                val start = startDate.atStartOfDayWithOffset(offset)
                val scheduledSlots = scheduleService.getByUserId(userId = call.getUserId(), start = start, days = days)
                call.respond(HttpStatusCode.OK, scheduledSlots.toDto(startDate = startDate, days = days))
            }
        }

        // Возвращает заброннированный слот по его id
        get<Api.SlotApi.Slot> {
            val slot = scheduleService.get(it.id)
            call.respond(HttpStatusCode.OK, slot.toDto())
        }

        // Возвращает список заброннированных слотов по id шаблона
        get<Api.SlotApi.ByTemplateId.Template> {
            val (days, startDate, offset) = call.receiveAndValidatePaginationParams()
            val start = startDate.atStartOfDayWithOffset(offset)
            val scheduledSlots = scheduleService.getByTemplateId(templateId = it.id, start = start, days = days)
            call.respond(HttpStatusCode.OK, scheduledSlots.toDto(startDate = startDate, days = days))
        }
    }

    private fun ApplicationCall.receiveAndValidatePaginationParams(): PaginationParams {
        val days = receiveAndValidateParam<Int>(name = DAYS_PARAM, maxValue = 7, minValue = 1, default = 1)
        val startDate = receiveParam<LocalDate>(name = START_DATE_PARAM)
        val offset = receiveParam<ZoneOffset>(name = ZONE_OFFSET_PARAM)
        return PaginationParams(days = days, startDate = startDate, offset = offset)
    }
}
