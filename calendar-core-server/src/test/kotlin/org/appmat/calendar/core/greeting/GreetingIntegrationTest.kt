package org.appmat.calendar.core.greeting

import io.kotest.assertions.ktor.shouldHaveContent
import io.kotest.assertions.ktor.shouldHaveStatus
import io.kotest.core.spec.style.StringSpec
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import org.appmat.calendar.core.util.withAppIntegrationTest

class GreetingIntegrationTest : StringSpec({
    "hello endpoint returns greeting".config(enabled = false) {
        withAppIntegrationTest {
            handleRequest(HttpMethod.Get, "/api/greeting").apply {
                response shouldHaveStatus HttpStatusCode.OK
                response shouldHaveContent "Hello World!"
            }
        }

    }
})
