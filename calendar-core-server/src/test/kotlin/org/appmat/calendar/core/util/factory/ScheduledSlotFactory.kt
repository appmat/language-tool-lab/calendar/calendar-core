package org.appmat.calendar.core.util.factory

import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.long
import io.kotest.property.arbitrary.uuid
import org.appmat.calendar.core.schedule.dto.ScheduledSlot
import org.appmat.calendar.core.util.factory.DateTimeIntervalFactory.dateTimeInterval

object ScheduledSlotFactory {
    fun scheduledSlot(templateId: Long?) = arbitrary {
        ScheduledSlot(
            id = Arb.long(min = 0).bind(),
            dateTimeInterval = dateTimeInterval().bind(),
            participantId = Arb.uuid().bind().toString(),
            meetingTemplateId = templateId?.let { templateId } ?: Arb.long().bind()
        )
    }
}
