package org.appmat.calendar.core.meeting_template

import io.kotest.core.extensions.Extension
import io.kotest.koin.KoinExtension
import io.kotest.property.arbitrary.next
import org.appmat.calendar.core.meeting_template.modules.meetingTemplateModule
import org.appmat.calendar.core.schedule.modules.scheduleModule
import org.appmat.calendar.core.schedule.services.ScheduleService
import org.appmat.calendar.core.util.BaseTestClassWithDB
import org.appmat.calendar.core.util.factory.meetingTemplate
import org.koin.test.KoinTest
import org.koin.test.inject

class ScheduleControllerTest : BaseTestClassWithDB(), KoinTest {

    override fun extensions(): List<Extension> = listOf(KoinExtension(listOf(scheduleModule, meetingTemplateModule)))

    val meetingTemplateService by inject<org.appmat.calendar.core.meeting_template.services.MeetingTemplateService>()
    val scheduleService by inject<ScheduleService>()

    init {
        "available slots".config(enabled = false) {
            meetingTemplateService.create(meetingTemplate().next())
        }
    }
}
