package org.appmat.calendar.core.util.factory

import org.appmat.calendar.core.meeting_template.dto.Interval as IntervalDto
import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import io.kotest.property.arbitrary.list
import io.kotest.property.arbitrary.localDate
import io.kotest.property.arbitrary.long
import io.kotest.property.arbitrary.next
import io.kotest.property.arbitrary.orNull
import io.kotest.property.arbitrary.string
import io.kotest.property.arbitrary.uuid
import java.time.LocalDate
import kotlin.random.Random
import kotlin.time.Duration
import org.appmat.calendar.core.common.data.MeetingTemplate
import org.appmat.calendar.core.common.data.WeekdayTimeInterval
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplateSaveRequest
import org.appmat.calendar.core.util.factory.DurationFactory.duration
import org.appmat.calendar.core.util.factory.WeekdayTimeIntervalFactory.weekdayTimeInterval

fun meetingTemplateSaveRequest(
    title: String = "Random title ${Random.nextLong()}",
    description: String? = if (Random.nextBoolean()) null else "Random description ${Random.nextLong()}",
    place: String? = if (Random.nextBoolean()) null else "Random place ${Random.nextLong()}",
    durationMin: Int = Random.nextInt(1, 1441),
    frequency: Int = Random.nextInt(1, 11),
    startWeek: LocalDate = LocalDate.now().plusDays(Random.nextLong(8)),
    intervals: List<IntervalDto> = emptyList(),
) = MeetingTemplateSaveRequest(
    title = title,
    description = description,
    place = place,
    durationMin = durationMin,
    repeatEveryNthWeek = frequency,
    startDate = startWeek,
    intervals = intervals,
)

fun meetingTemplate(
    id: Long = Arb.long().next(),
    userId: String = Arb.uuid().next().toString(),
    title: String = Arb.string().next(),
    description: String? = Arb.string().orNull().next(),
    place: String? = Arb.string().orNull().next(),
    duration: Duration = duration().next(),
    repeatEveryNthWeek: Int = Arb.int(1..4).next(),
    startDay: LocalDate = Arb.localDate().next(),
    link: String? = null,
    weekdayTimeIntervals: List<WeekdayTimeInterval> = Arb.list(weekdayTimeInterval()).next(),
) = arbitrary {
    MeetingTemplate(
        id = id,
        userId = userId,
        title = title,
        description = description,
        place = place,
        duration = duration,
        repeatEveryNthWeek = repeatEveryNthWeek,
        startDay = startDay,
        link = link,
        weekdayTimeIntervals = weekdayTimeIntervals,
    )
}

