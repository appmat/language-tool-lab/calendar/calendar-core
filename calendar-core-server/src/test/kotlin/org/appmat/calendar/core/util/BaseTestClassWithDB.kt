package org.appmat.calendar.core.util

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.kotest.core.spec.BeforeEach
import io.kotest.core.spec.style.StringSpec
import java.io.File
import java.nio.file.Paths
import kotlin.io.path.absolutePathString
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.FileSystemResourceAccessor
import org.jetbrains.exposed.sql.Database
import org.testcontainers.containers.PostgreSQLContainer

val cleanDb: BeforeEach = { }

abstract class BaseTestClassWithDB(tests: StringSpec.() -> Unit = {}) : StringSpec({

    beforeEach(cleanDb)

    val databaseImage = PostgreSQLContainer("postgres:14")
        .withDatabaseName("test")
        .withUsername("test")
        .withPassword("test")

    databaseImage.start()

    val hikariDataSource = HikariDataSource(
        HikariConfig().apply {
            username = databaseImage.username
            password = databaseImage.password
            jdbcUrl = databaseImage.jdbcUrl
            driverClassName = databaseImage.driverClassName
        }
    )

    val jdbcConnection = JdbcConnection(hikariDataSource.connection)
    val database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection)
    // TODO Нормальное объявление пути
    val liquibase = Liquibase(
        "../liquibase/dbchangelog.xml",
        FileSystemResourceAccessor(File(Paths.get("").absolutePathString())),
        database
    )
    liquibase.update("")
    liquibase.close()

    Database.connect(hikariDataSource)

    tests()
})
