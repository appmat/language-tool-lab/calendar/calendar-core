package org.appmat.calendar.core.util.factory

import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.int
import kotlin.time.Duration.Companion.minutes

object DurationFactory {
    fun duration() = arbitrary { Arb.int(1, 1440).bind().minutes }
}
