package org.appmat.calendar.core.meeting_template

import io.kotest.assertions.ktor.shouldHaveStatus
import io.kotest.extensions.testcontainers.configurePerTest
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import java.time.LocalDate
import kotlin.random.Random
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.appmat.calendar.core.meeting_template.dto.Interval
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplate
import org.appmat.calendar.core.meeting_template.dto.MeetingTemplateSaveRequest
import org.appmat.calendar.core.util.BaseTestClassWithDB
import org.appmat.calendar.core.util.factory.interval
import org.appmat.calendar.core.util.factory.meetingTemplateSaveRequest
import org.appmat.calendar.core.util.withAppIntegrationTest

class MeetingTemplateControllerTest : BaseTestClassWithDB({

    "should return not found on not existing template id".config(enabled = false) {
        withAppIntegrationTest {
            val id = Random.nextLong(Long.MAX_VALUE)
            handleRequest(HttpMethod.Get, "/api/meeting-template/$id").apply {
                response shouldHaveStatus HttpStatusCode.NotFound
            }
        }
    }
    "should return not found on not numeric id".config(enabled = false) {
        withAppIntegrationTest {
            handleRequest(HttpMethod.Get, "/api/meeting-template/abc").apply {
                response shouldHaveStatus HttpStatusCode.BadRequest
            }
        }
    }
    "should save meeting template".config(enabled = false) {
        val meetingTemplateSaveRequest =
            meetingTemplateSaveRequest(durationMin = 10, intervals = listOf(interval(duration = 20)))
        val call = postRequest(meetingTemplateSaveRequest = meetingTemplateSaveRequest)
        call.response shouldHaveStatus HttpStatusCode.Created
        call.response.content.shouldNotBeNull()
        val meetingTemplate = Json.decodeFromString<MeetingTemplate>(call.response.content!!)
        meetingTemplate.title shouldBe meetingTemplateSaveRequest.title
        meetingTemplate.description shouldBe meetingTemplateSaveRequest.description
        meetingTemplate.place shouldBe meetingTemplateSaveRequest.place
        meetingTemplate.durationMin shouldBe meetingTemplateSaveRequest.durationMin
        meetingTemplate.repeatEveryNthWeek shouldBe meetingTemplateSaveRequest.repeatEveryNthWeek
        meetingTemplate.startDate shouldBe meetingTemplateSaveRequest.startDate
        meetingTemplate.intervals shouldHaveSize meetingTemplateSaveRequest.intervals.size
        meetingTemplate.intervals.first() shouldBe meetingTemplateSaveRequest.intervals.first()
    }
    "should save meeting template and merge intervals".config(enabled = false) {
        val meetingTemplateSaveRequest = meetingTemplateSaveRequest(
            durationMin = 90,
            intervals = listOf(
                interval(start = 900, duration = 120, weekday = Interval.Weekday.MONDAY),
                interval(start = 900, duration = 120, weekday = Interval.Weekday.MONDAY),
                interval(start = 800, duration = 120, weekday = Interval.Weekday.MONDAY),
                interval(start = 1000, duration = 120, weekday = Interval.Weekday.MONDAY),
                interval(start = 600, duration = 120, weekday = Interval.Weekday.FRIDAY),
                interval(start = 600, duration = 120, weekday = Interval.Weekday.MONDAY),
                interval(start = 1000, duration = 120, weekday = Interval.Weekday.FRIDAY),
            )
        )
        val request = postRequest(
            meetingTemplateSaveRequest
        ).apply {
            response shouldHaveStatus HttpStatusCode.Created
        }
        request.response.content.shouldNotBeNull()
        val meetingTemplate = Json.decodeFromString<MeetingTemplate>(request.response.content!!)
        meetingTemplate.title shouldBe meetingTemplateSaveRequest.title
        meetingTemplate.description shouldBe meetingTemplateSaveRequest.description
        meetingTemplate.place shouldBe meetingTemplateSaveRequest.place
        meetingTemplate.durationMin shouldBe meetingTemplateSaveRequest.durationMin
        meetingTemplate.repeatEveryNthWeek shouldBe meetingTemplateSaveRequest.repeatEveryNthWeek
        meetingTemplate.startDate shouldBe meetingTemplateSaveRequest.startDate
        meetingTemplate.intervals shouldHaveSize 4
        meetingTemplate.intervals shouldContain interval(start = 600, duration = 120, Interval.Weekday.MONDAY)
        meetingTemplate.intervals shouldContain interval(start = 800, duration = 320, Interval.Weekday.MONDAY)
        meetingTemplate.intervals shouldContain interval(start = 600, duration = 120, Interval.Weekday.FRIDAY)
        meetingTemplate.intervals shouldContain interval(start = 1000, duration = 120, Interval.Weekday.FRIDAY)

        // todo assert database records
    }
    "duration should be greater or equal 0".config(enabled = false) {
        val response = postRequest(meetingTemplateSaveRequest(durationMin = -1)).response
        response shouldHaveStatus HttpStatusCode.BadRequest
    }
    "duration should be less or equal 1440".config(enabled = false) {
        val response = postRequest(meetingTemplateSaveRequest(durationMin = 1441)).response
        response shouldHaveStatus HttpStatusCode.BadRequest
    }
    "frequency should be greater than 0".config(enabled = false) {
        val response = postRequest(meetingTemplateSaveRequest(frequency = 0)).response
        response shouldHaveStatus HttpStatusCode.BadRequest
    }
    // todo add tests
})

private fun postRequest(
    meetingTemplateSaveRequest: MeetingTemplateSaveRequest
) = postRequest(body = Json.encodeToString(meetingTemplateSaveRequest))

private fun postRequest(
    body: String
) = withAppIntegrationTest {
    handleRequest(HttpMethod.Post, "/api/meeting-template") {
        addHeader("Content-Type", "application/json")
        setBody(body)
    }
}
