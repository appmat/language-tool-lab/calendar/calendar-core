package org.appmat.calendar.core.util

import com.typesafe.config.ConfigFactory
import io.ktor.config.ApplicationConfig
import io.ktor.config.HoconApplicationConfig
import io.ktor.server.testing.TestApplicationEngine
import io.ktor.server.testing.createTestEnvironment
import io.ktor.server.testing.withApplication
import org.appmat.calendar.core.module


fun <R> withAppIntegrationTest(test: TestApplicationEngine.() -> R) =
    withApplication(createTestEnvironment {
        config = HoconApplicationConfig(ConfigFactory.load("application.conf"))
        module {
            module(testing = true)
        }
    }) {
        test()
    }
