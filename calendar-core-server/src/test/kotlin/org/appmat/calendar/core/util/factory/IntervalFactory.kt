package org.appmat.calendar.core.util.factory

import java.time.Instant
import java.time.OffsetTime
import java.time.ZoneOffset
import kotlin.random.Random
import kotlin.time.Duration.Companion.minutes
import org.appmat.calendar.core.common.data.TimeInterval
import org.appmat.calendar.core.meeting_template.dto.Interval

fun interval(
    start: Long = Random.nextLong(1441),
    duration: Long = Random.nextLong(0, 1441 - start),
    weekday: Interval.Weekday = Interval.Weekday.values().random()
) = Interval(
    timeInterval = TimeInterval(
        startTime = OffsetTime.ofInstant(Instant.ofEpochSecond(start * 60), ZoneOffset.UTC),
        duration = duration.minutes,
    ),
    weekday = weekday,
)
