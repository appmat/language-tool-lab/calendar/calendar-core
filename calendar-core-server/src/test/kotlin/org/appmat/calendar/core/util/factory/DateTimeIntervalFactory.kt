package org.appmat.calendar.core.util.factory

import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.instant
import io.kotest.property.arbitrary.timezoneCodeThree
import java.time.OffsetDateTime
import java.time.ZoneId.SHORT_IDS
import java.time.ZoneId.of
import org.appmat.calendar.core.common.data.DateTimeInterval
import org.appmat.calendar.core.util.factory.DurationFactory.duration

object DateTimeIntervalFactory {
    fun dateTimeInterval() = arbitrary {
        DateTimeInterval(
            startDateTime = OffsetDateTime.ofInstant(
                Arb.instant().bind(),
                of(Arb.timezoneCodeThree().bind(), SHORT_IDS)
            ),
            duration = duration().bind()
        )
    }
}
