package org.appmat.calendar.core.util.factory

import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.instant
import io.kotest.property.arbitrary.next
import java.time.OffsetTime
import java.time.ZoneId
import java.time.ZoneId.SHORT_IDS
import kotlin.time.Duration
import org.appmat.calendar.core.common.data.TimeInterval
import org.appmat.calendar.core.util.factory.DurationFactory.duration

object TimeIntervalFactory {
    fun timeInterval(
        startTime: OffsetTime = OffsetTime.ofInstant(
            Arb.instant().next(),
            ZoneId.of(SHORT_IDS.keys.random(), SHORT_IDS)
        ),
        duration: Duration = duration().next(),
    ) = arbitrary {
        TimeInterval(
            startTime = startTime,
            duration = duration,
        )
    }
}
