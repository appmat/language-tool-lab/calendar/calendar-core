package org.appmat.calendar.core.util.factory

import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.enum
import org.appmat.calendar.core.common.data.Weekday
import org.appmat.calendar.core.common.data.WeekdayTimeInterval
import org.appmat.calendar.core.util.factory.TimeIntervalFactory.timeInterval

object WeekdayTimeIntervalFactory {
    fun weekdayTimeInterval() = arbitrary {
        WeekdayTimeInterval(
            timeInterval = timeInterval().bind(),
            weekday = Arb.enum<Weekday>().bind(),
        )
    }
}
