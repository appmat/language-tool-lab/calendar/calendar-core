--liquibase formatted sql

--changeset zheg-al:meeting_template_initial_changeset
create table if not exists meeting_template
(
    id                    BIGSERIAL primary key,
    user_id               varchar(255),
    title                 varchar(255) not null,
    description           varchar(1000),
    place                 text,
    duration_min          integer,
    repeat_every_nth_week integer,
    start_day             date,
    link                  varchar(1000)
);

--changeset zheg-al:interval_initial_changeset
create table if not exists interval
(
    id                  BIGSERIAL primary key,
    meeting_template_id bigint references meeting_template (id) on delete cascade on update cascade,
    start_time          text,   -- time with timezone in ISO 8601 format
    duration            text,   -- in ISO 8601 format
    week_day            varchar(9)
);
