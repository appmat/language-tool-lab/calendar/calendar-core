--liquibase formatted sql

--changeset zheg-al:scheduled_meeting_initial_changeset
create table if not exists scheduled_meeting
(
    id                  BIGSERIAL primary key,
    meeting_template_id bigint references meeting_template (id) on delete restrict on update cascade,
    participant_id      text not null,
    start_date_time     timestamp with time zone,
    duration            text
);
